DROP TABLE IF EXISTS  USERS;

CREATE TABLE USERS(
         username TEXT PRIMARY_KEY,
         first_name TEXT,
         last_name TEXT);