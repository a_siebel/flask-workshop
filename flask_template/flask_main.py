from flask import Flask
from flask import request
from flask import render_template
from usermodel import UserFactory

app = Flask(__name__)

@app.route("/")
def hello_world():
    return """<html>
                <head><title>Index</title></head>
                <body><h1>Index</h1></body>
              </html>"""

@app.route("/templating_example")
def templating_example():
        return render_template(
        'content.html',
        title="Content",
        description="My Jinja Content")

@app.route("/user/<string:username>")
def user(username):
    user = UserFactory("database.db").user_from_db(username)
    if user:
        return "Hello " +  user.first_name
    else:
        return "No user found"

@app.route('/add_user', methods=['GET', 'POST'])
def form_example():
    if request.method == "GET":
        return '''
                  <form method="POST">
                      <div><label>Username: <input type="text" name="username"></label></div>
                      <div><label>Firstname: <input type="text" name="first_name"></label></div>
                      <div><label>Lastname: <input type="text" name="last_name"></label></div>
                      <input type="submit" value="Submit">
                  </form>'''
    else:
        username = request.form.get("username")
        firstname = request.form.get("first_name")
        lastname = request.form.get("last_name")
        UserFactory("database.db").create_user(username, firstname, lastname)
        return f"Benutzer {username} wurde hinzugefügt"
        
        