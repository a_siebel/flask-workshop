import sqlite3

class User:
    def __init__(self, username, first_name, last_name):
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        
        
class UserFactory:
    def __init__(self, file):
        self.file = file
        
    def create_db(self):
        with open('schema.sql') as f:
            connection = sqlite3.connect(self.file)
            connection.executescript(f.read())
            connection.commit()
            connection.close()

    def user_from_db(self, username):
        conn = sqlite3.connect(self.file)
        cursor = conn.cursor()
        sql = f'SELECT username, first_name, last_name FROM USERS WHERE username =  "{username}"'
        cursor.execute(sql)
        row = cursor.fetchone()
        conn.close()
        if row:
            username = row[0]
            firstname = row[1]
            lastname = row[2]
            r_value = User(username, firstname, lastname)
        else:
            r_value = None
        return r_value
    
    def create_user(self, username, first_name, last_name):
        connection = sqlite3.connect(self.file)
        cursor = connection.cursor()
        sql = f"INSERT INTO USERS (username, first_name, last_name) VALUES ('{username}', '{first_name}', '{last_name}')"
        cursor.execute(sql)
        connection.commit()
        connection.close()
        
        

if __name__ == "__main__":
    factory = UserFactory("database.db")
    factory.create_db()
    factory.create_user("asiebel", "Andreas", "Siebel")
    
    
