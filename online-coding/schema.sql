DROP TABLE IF EXISTS USERS;

CREATE TABLE USERS(
    username TEXT PRIMARY_KEY,
    firstname TEXT,
    lastname TEXT);