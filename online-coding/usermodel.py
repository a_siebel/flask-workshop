import sqlite3
from typing import Union

class User:
    def __init__(self, username, firstname, lastname):
        self.username = username
        self.firstname = firstname
        self.lastname = lastname
    
       
class UserFactory:
    def __init__(self, file):
        self.file = file
        
    def create_db(self):
        with open("schema.sql") as schema:
            connection = sqlite3.connect(self.file)
            connection.executescript(schema.read())
            connection.commit()
            connection.close()
            
    def create_user(self, user : User):
        connection = sqlite3.connect(self.file)
        cursor = connection.cursor()
        sql = f"INSERT INTO USERS (username, firstname, lastname) VALUES ('{user.username}','{user.firstname}', '{user.lastname}')"        
        print(sql)
        cursor.execute(sql)
        connection.commit()
        connection.close()
        
    def user_from_db(self, username: str) -> Union[User, None]:
        connection = sqlite3.connect(self.file)
        cursor = connection.cursor()
        sql = f'SELECT username, firstname, lastname FROM USERS WHERE username = "{username}"'
        print(sql)
        cursor.execute(sql)
        row = cursor.fetchone()
        connection.close()
        if row:
            username = row[0]
            firstname = row[1]
            lastname = row[2]
            user = User(username, firstname, lastname)
            return user
        else:
            return None
        
        
if __name__ == "__main__":
    factory = UserFactory("database.db")
    factory.create_db()
    factory.create_user(User("asiebel", "Andreas", "Siebel"))
    user = factory.user_from_db("asiebel")
    print(user.username, user.firstname, user.lastname)
