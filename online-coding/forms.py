from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class UserCreationForm(FlaskForm):
    username = StringField("username", [DataRequired()])
    firstname = StringField("Vorname", [DataRequired()])
    lastname = StringField("Nachname", [DataRequired()])
    submit = SubmitField("Absenden")