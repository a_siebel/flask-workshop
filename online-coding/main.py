from flask import Flask
from flask import request
from flask import render_template
import usermodel
import forms

app = Flask(__name__)
app.config['SECRET_KEY'] = "dasduoirwhbidasmj"

@app.route("/")
def hello_world():
    return """<html>
                <head><title>Hello</title></head>
                <body><h1>Hello World</h1></body>
              </html>"""

@app.route("/hallo")
def hello():
    return """<html>
                <head><title>Hello</title></head>
                <body><h1>Hello</h1></body>
              </html>"""

@app.route("/hallo/<string:username>")
def hello_username(username):
    return f"""<html>
                <head><title>Hello</title></head>
                <body><h1>Hello {username} </h1></body>
              </html>"""

@app.route("/templating")
def templating_example():
    return render_template(
        "content.html",
        title="Content",
        description="My Description")


@app.route("/user/<string:username>")
def show_user(username):
    user = usermodel.UserFactory("database.db").user_from_db(username)
    if user:
        return "Hello" + user.firstname
    else:
        return "No user found"
    

@app.route("/add_user", methods=["GET", "POST"])
def user_form():
    if request.method == "GET":
        return '''<form method="POST">
                      <div><label>Username: <input type="text" name="username"></label></div>
                      <div><label>Firstname: <input type="text" name="firstname"></label></div>
                      <div><label>Lastname: <input type="text" name="lastname"></label></div>
                      <input type="submit" value="Submit">
                  </form>'''
    elif request.method == "POST":
        username = request.form.get("username")
        firstname = request.form.get("firstname")
        lastname = request.form.get("lastname")
        user = usermodel.User(username, firstname, lastname)
        usermodel.UserFactory("database.db").create_user(user)
        return f"Benutzer {username} wurde angelegt"
        

@app.route("/add_user_wtf", methods=["GET", "POST"])
def user_form_wtf():
    form = forms.UserCreationForm()
    validated = form.validate_on_submit()
    if validated:
        username = form.username.data
        firstname = form.firstname.data
        lastname = form.lastname.data
        user = usermodel.User(username, firstname, lastname)
        usermodel.UserFactory("database.db").create_user(user)
        return f"Benutzer {username} wurde angelegt"
    else:
        return f'''<form method="POST">
                      <div>Username: {form.username}</div>
                      <div>Firstname: {form.firstname}</div>
                      <div>Lastname: {form.lastname}</div>
                      { form.csrf_token }
                      {form.submit}
                  </form>'''

    
    
    
    
    
    
    
    

