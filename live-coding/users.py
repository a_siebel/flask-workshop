import sqlite3

class User:
    def __init__(self, username):
        self.username = username
        
class UserFactory:
    def __init__(self, file):
        self.file = file
        
    def create_db(self):
        with open("schema.sql") as f:
            con = sqlite3.connect(self.file)
            con.executescript(f.read())
            con.commit()
            con.close()

    def user_from_db(self, username):
        con = sqlite3.connect(self.file)
        cursor = con.cursor
        sql = f"SELECT username FROM users WHERE username = {username}"
        cursor.execute(sql)
        row = cursor.fetchone()
        username = row[0]
        con.close()
        return username
        
if __name__ == "main":
    factory = UserFactory("database.db")
    factory.create_db()
        
        
    