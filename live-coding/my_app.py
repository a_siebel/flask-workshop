from flask import Flask
from flask import request

app = Flask(__name__)

@app.route("/")
def hello_world():
    return """Hello <b>W</b>orld"""

@app.route("/user/<string:username>")
def show_user(username):
    return f"User {username} wird angezeigt"