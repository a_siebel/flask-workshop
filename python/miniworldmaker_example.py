import miniworldmaker

def main():
    board = miniworldmaker.TiledBoard()
    board.columns = 20
    board.rows = 8
    board.tile_size = 42
    board.add_background("images/soccer_green.jpg")
    board.speed = 30
    player = miniworldmaker.Token()
    player.add_costume("images/player.png")
    player.costume.orientation = -90
    @player.register
    def act(self):
        self.direction = 90
        self.move()

    board.run()
main()