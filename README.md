# Materialien Flask Workshop

Am 24.11 veranstaltete die Fachgruppe HRPI den Informatiktag in Frankfurt. Dort habe den Workshop "Python und Flask" geleitet.

Da -wie so oft- die Zeit etwas kurz war, habe ich den Workshop mit digitalem Material ergänzt. Du findest hier Videos zu Python und demnächst auch Videos zu Flask.

### Python

  * [Python - Teil 1 - Warum Python?](https://youtu.be/hbD7-ajfM5g)

  * [Python - Teil 2 - Start in Python](https://youtu.be/Xt86i4eGtxM)
  
  * [Python - Teil 3 - Datentypen](https://youtu.be/rwYsM3DhOFg)
  
  * [Python - Teil 4 - Listen, Verzweigungen und Schleifen](https://youtu.be/nWXPbI2UZ7Y)
  
  * [Python - Teil 5 - Objekte und Klassen](https://youtu.be/Xhula4j60ig)
  
  * [Python - Teil 6 - Anwendungen](https://youtu.be/Apx98ErduU0)
  
### Flask - Webdatenbankprojekt

  * [Flask - Teil 1 - Warum Flask?](https://youtu.be/4ElEX32xuJE)

  * [Flask - Teil 2 - Hello World](https://youtu.be/vGP0C7KwWzA)

  * [Flask - Teil 3 - Routen](https://youtu.be/erv2oIFlybw)

  * [Flask - Teil 4 - Templating](https://youtu.be/owj2al4y4RI)

  * [Flask - Teil 5 - Das Datenbankmodell / Die Datenbank anlegen](https://youtu.be/JdatTwdJGYk)

  * [Flask - Teil 6 - Datensätze lesen und anlegen](https://youtu.be/0rwe7s9wDxc)

  * [Flask - Teil 7a - Exkurs: Vor dem importieren...](https://youtu.be/xHUSSdGfKmM)
  
  * [Flask - Teil 7b - Exkurs Factory Methods](https://youtu.be/uP_wa475B_0) (kann übersprungen werden...)

  * [Flask - Teil 8 - Daten aus der Datenbank auslesen](https://youtu.be/Wtxbty155PY)

  * [Flask - Teil 9 - Daten in die Datenbank schreiben (Formulare, Requests, POST und GET)](https://youtu.be/8uFaO4jJ-ok)

  * [Flask - Teil 10 - Formulare mit WTF](https://youtu.be/NVSpyhlG1QE)

  * [Flask - Teil 11 - Ist ein Webdatenbankprojekt überhaupt sinnvoll oder gibt es andere Alternativen?](https://youtu.be/FTdjaHSz6zo)

### Geplante Inhalte:

  * Flask und SQLAlchemy

  * Flask und Plotly

### Audioqualität 

Eine kleine Entschuldigung vorweg: Ich habe leider zwischendurch mein Setup hier zu Hause geändert und habe die Videos daher mit unterschiedlichen  Mikrophonen und Einstellungen aufgenommen - Die Audioqualität schwankt daher etwas. Ich hoffe das man die Videos trotzdem noch gut hören kann.

## Weitere Python-Materialien

  * [trinket.io](https://github.com/micropython/micropython?utm_source=mybridge&utm_medium=blog&utm_campaign=read_more) - Python online abspielen
  
  * Alternativ zu trinket.io kann man auch [repl.it](repl.it) verwenden um Python Code online auszuführen. [Hier](https://learnpythontherightway.com/)) gibt es ein Buch mit Tutorials, welche sich auf repl.it beziehen
    
  * [Frameworks](https://github.com/vinta/awesome-python) - Python Frameworks für verschiedene Anwendungsbereiche
   
  * [miniworldmaker.de](https://miniworldmaker.de/) - Meine 2D-Grafik Engine als Alternative zu [PyGame Zero](https://pygame-zero.readthedocs.io/en/stable/), [repl.it play](https://github.com/replit/play) und [Python Arcade](https://api.arcade.academy/en/latest/)
  
  * [Computer Science Curriculum](https://academy.cs.cmu.edu/) - Ein Informatik-Curriculum der Carnegie Mellon University, welches [hier]
  
  * [Moodle - Python](https://projects.opencoding.de/informatikmoodle/der-python-kurs/) - Mein Python-Kurs mit Moodle und Coderunner.
  
  * [Snakify](https://www.snakify.org/en/) - Ein Online-Python Kurs mit interaktiven Übungen
  
  * [inf-schule](https://www.inf-schule.de/oop/python) - Inf-schule hat ein Modul zur objektorientierten Programmierung mit Python.
  
  * [cscircles](https://cscircles.cemc.uwaterloo.ca/de/) - Der Programmierkurs vom Team des BWInf.
  
  * [How to think like a computer scientist](https://runestone.academy/ns/books/published/thinkcspy/index.html) ist ein tolles (englisches) Buch mit zahlreichen interaktiven Übungen.
  
## Struktur

  * flask_template: Mein ursprüngliches Flask Template

  * live coding: Material, welches im Workshop entstanden ist.
  
  * presentation: Die Slides der Präsentation
  
  * python: Ein miniworldmaker Beispiel
  
 
